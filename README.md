## PROJECT ##

* ID: **M**anga **P**ack **C**reator
* Version: **0.1.1**
* OS: Linux
* Contact: contact@schepsen.eu


## USAGE ##

* python3 mpc.py MANGA <CH> <E-CH-FLAG>

## CHANGELOG ##

### Manga Pack Creator v. 0.1.1, 2015-03-04 ###

* UI fixed & optimized

### Manga Pack Creator v. 0.1.0, 2015-03-04 ###

* Initial version