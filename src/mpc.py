#!/usr/bin/python3

from sys import argv, exit
from os  import listdir, path, rename, system

# PROJECT INFORMATION

VERSION = '0.1.1'

# RENAMING TEMPLATES

TMPL_PAGE = '%s (%0.2d).%s'
# Example: Tokyo Ghoul # 001 (01).png

TMPL_CH = '%s # %0.3d'
# Example: Tokyo Ghoul # 001

TMPL_CH_EXTRA = '%s # Extra %0.2d'
# Example: Tokyo Ghoul # Extra 01

def getContent(directory):
    return sorted(list(filter(lambda x: not x.startswith('.'), listdir(directory))))
    
def main():
    
    # HANDLING ARGVs

    if len(argv) >= 2:
                               
        if argv[1] == '-v' or argv[1] == '--version':
                        
            exit('MPC version %s' % VERSION)
        
        # What is the name of the manga?
        manga = argv[1]    
        # What is the starting chapter?
        num = int(argv[2]) if len(argv) == 3 else 1
        # Is there any extra chapters? 
        extra = True if len(argv) == 4 else False
          
    else:       
                         
        exit('Usage: python3 %s MANGA <CH> <E-CH-FLAG>' % (argv[0]))      
           
    # CHAPTERS
    
    for chapter in getContent('%s' % manga): 
        
        PATH_CHAPTER = '%s/%s' % (manga, chapter)  # path to the current chapter  
        
        if path.isdir(PATH_CHAPTER):                      
            chapter = TMPL_CH % (manga, num) if not extra else TMPL_CH_EXTRA % (manga, num)  
            
            rename(PATH_CHAPTER, '%s/%s' % (manga, chapter))   
                           
            page = 1
            # That is the newly renamed path to the chapter  
            PATH_CHAPTER = '%s/%s' % (manga, chapter)                    
            
            # PAGES
            
            for image in getContent(PATH_CHAPTER):    
                if image.endswith(('.jpg', '.png')):  
                    
                    rename('%s/%s' % (PATH_CHAPTER, image), ('%s/' + TMPL_PAGE) % (PATH_CHAPTER, chapter, page, image[-3:]))
                    
                    page += 1 
                    
            system('zip -r -j "' + PATH_CHAPTER + '.zip" "' + PATH_CHAPTER + '" > /dev/null 2>&1')
            system('rm -rf "' + PATH_CHAPTER + '"') 
            
            print('Creating Chapter # %0.3d ... DONE!' % num)
            
            num += 1     
 
    print('All content was renamed successfully!', end='\n')  
    
if __name__ == "__main__": 
    main()
